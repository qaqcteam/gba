package com.example.reiromekylejoval.gbafunctionaltestautomation.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by reirome.joval on 2/14/2018.
 */

public class RetrofitClient {

    public static final String BASE_URL = "http://10.10.10.30:80/gba-webservice-1.0/public/";

    public static APIService getClient() {
        return new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(APIService.class);
    }
}
