package com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects;

/**
 * Created by reirome.joval on 2/13/2018.
 */
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OnboardingPageObject {


    public OnboardingPageObject(AndroidDriver androidDriver) {
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver, 5, TimeUnit.SECONDS), this);
    }

    @AndroidFindBy(id = "btn_skip")
    public AndroidElement btnSkip;

    @AndroidFindBy(id = "view_pager")
    public AndroidElement view_pager;
}