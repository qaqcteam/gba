package com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects;

import com.example.reiromekylejoval.gbafunctionaltestautomation.module.ForgotPassword;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by reirome.joval on 2/14/2018.
 */



public class VerificationPageObject {
    public VerificationPageObject() {

    }

    public VerificationPageObject(AndroidDriver androidDriver){
        System.out.printf(androidDriver.getPlatformName());
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver, 5, TimeUnit.SECONDS), this);
    }

    @AndroidFindBy(id = "edtPinEntry")
    public AndroidElement edtPinEntry;

    @AndroidFindBy(id = "tvResendCode")
    public AndroidElement tvResendCode;

    public NewPasswordPageObject navigateToNewPassword(String code, AndroidDriver androidDriver){
        edtPinEntry.sendKeys(code);
        //WebDriverWait wait = new WebDriverWait(androidDriver, 30).until(ExpectedConditions.elementToBeClickable())
        return new NewPasswordPageObject(androidDriver);

    }


}
