package com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

/**
 * Created by reirome.joval on 2/19/2018.
 */

public class SingletonFactory {

        private static SingletonFactory instance = new SingletonFactory();
        private static AndroidDriver driver;

        private SingletonFactory() {
        }

        // Get the only object available
        public static SingletonFactory getInstance() {
            return instance;
        }

        // Get the only object available
        public void setDriver(AndroidDriver driver1) {
            driver = driver1;
        }

        public AndroidDriver getAppiumDriver() {
            return driver;
        }

    }
