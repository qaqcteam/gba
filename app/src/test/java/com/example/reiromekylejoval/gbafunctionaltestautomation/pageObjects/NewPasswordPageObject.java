package com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects;

import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by reirome.joval on 2/19/2018.
 */

public class NewPasswordPageObject {
    public NewPasswordPageObject(AndroidDriver androidDriver) {
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver, 5,TimeUnit.SECONDS), this);
    }

    @AndroidFindBy(id = "edtPassword")
    public AndroidElement edtPassword;

    @AndroidFindBy(id = "edtConfirmPassword")
    public AndroidElement edtConfirmPassword;

    @AndroidFindBy(id = "btnSubmit")
    public AndroidElement btnSubmit;

    public SuccessPageObject NavigateToSuccess(AndroidDriver andriodDriver){
        btnSubmit.click();
        return new SuccessPageObject(andriodDriver);
    }


}
