package com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by reirome.joval on 2/14/2018.
 */

public class ForgotPasswordAPIEntities {


    @SerializedName("mobile")
    @Expose
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}



