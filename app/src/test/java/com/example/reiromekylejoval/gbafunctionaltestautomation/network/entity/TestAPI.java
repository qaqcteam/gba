package com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by reirome.joval on 2/14/2018.
 */

public class TestAPI {

    @SerializedName("message")
    public String message;

    @SerializedName("tmp")
    public Temp tmp;

}
