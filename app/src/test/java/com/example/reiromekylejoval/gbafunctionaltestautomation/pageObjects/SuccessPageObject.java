package com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects;

import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by reirome.joval on 2/19/2018.
 */

public class SuccessPageObject {

    public SuccessPageObject(AndroidDriver androidDriver) {
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver, 5, TimeUnit.SECONDS), this);
    }

    @AndroidFindBy(id = "btn_neutral")
    public AndroidElement btn_neutral;


}
