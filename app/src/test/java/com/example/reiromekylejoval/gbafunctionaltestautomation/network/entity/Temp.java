package com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by reirome.joval on 2/14/2018.
 */

public class Temp {

    @SerializedName("code")
    private String code;

    @SerializedName("expires_at")
    private String expires_at;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getExpires_at() {
        return expires_at;
    }

    public void setExpires_at(String expires_at) {
        this.expires_at = expires_at;
    }
}
