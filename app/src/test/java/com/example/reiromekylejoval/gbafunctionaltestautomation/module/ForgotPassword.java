package com.example.reiromekylejoval.gbafunctionaltestautomation.module;

import com.example.reiromekylejoval.gbafunctionaltestautomation.base.BaseActions;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.APIService;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.Utility;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity.TestAPI;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.ForgotPasswordPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.LoginPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.NewPasswordPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.SuccessPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.VerificationPageObject;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by reirome.joval on 2/15/2018.
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ForgotPassword extends BaseActions {

    @Test
    public void _1InvalidMobileNumber() {
        navigateToForgotPassword();
        ForgotPasswordPageObject fpPage = new ForgotPasswordPageObject(androidDriver);
        tap(fpPage.txtMobile);
        sendKeysTo(fpPage.txtMobile, "639282815131");


    }
    @Test
    public void _2validMobileNumber() {
        androidDriver.launchApp();
        ForgotPasswordPageObject fpPage = new ForgotPasswordPageObject(androidDriver);
        VerificationPageObject vPage = fpPage.navigateToVerification(androidDriver);
        getVerificationCode("+639262815131");
        NewPasswordPageObject npPage = vPage.navigateToNewPassword(code, androidDriver);
        sendKeysTo(npPage.edtPassword, "Welcome123");
        sendKeysTo(npPage.edtConfirmPassword, "Welcome123");
        SuccessPageObject successPage = npPage.NavigateToSuccess(androidDriver);
        tap(successPage.btn_neutral);
    }

    public void getVerificationCode(String mobile) {
        try {
            Response<TestAPI> test = mAPIService.getVerification(mobile).execute();
            if (test.isSuccessful()) {
                code = test.body().tmp.getCode();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
