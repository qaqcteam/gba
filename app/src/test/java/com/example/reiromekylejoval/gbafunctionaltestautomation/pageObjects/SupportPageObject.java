package com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by reirome.joval on 2/14/2018.
 */


public class SupportPageObject {

    public SupportPageObject(AndroidDriver androidDriver){
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver, 5, TimeUnit.SECONDS), this);
    }

    @AndroidFindBy(id = "btnContactSupport")
    public AndroidElement btnContactSupport;


}
