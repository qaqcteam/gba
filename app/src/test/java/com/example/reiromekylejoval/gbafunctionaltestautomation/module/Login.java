package com.example.reiromekylejoval.gbafunctionaltestautomation.module;

/**
 * Created by Reirome.Kyle.Joval on 11/17/2017.
 */

import android.nfc.Tag;
import android.util.Log;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.APIService;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.Utility;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity.TestAPI;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.ForgotPasswordPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.LoginPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.OnboardingPageObject;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.reiromekylejoval.gbafunctionaltestautomation.base.*;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.SupportPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.VerificationPageObject;

public class Login extends BaseActions {
    @Test
    public void onboarding() {
        swipeLeft();
        OnboardingPageObject page = new OnboardingPageObject(androidDriver);
        String skiptxt = page.btnSkip.getText();
        while (!skiptxt.equals("Done")) {
            if (!skiptxt.equals("Done")) {
                swipeLeft();
                skiptxt = page.btnSkip.getText();
            }
        }
        tap(page.btnSkip);
        System.out.print("Passed");
    }

    @Test
    public void NavigateToSupportForm(){
        onboarding();
        LoginPageObject support = new LoginPageObject(androidDriver);
        tap(support.tvSupp);
        SupportPageObject contactsupport = new SupportPageObject(androidDriver);
        tap(contactsupport.btnContactSupport);
        System.out.println("passed");


    }

    @Test
    public void Login() {
        LoginPageObject loginPageObject = new LoginPageObject(androidDriver);
        boolean onboardingElementExists = checkElementExists(By.id("username"));
        if (onboardingElementExists == false) {
            onboarding();
        }
        sendKeysTo(loginPageObject.username, "639262815131");
        sendKeysTo(loginPageObject.password, "Welcome123");
        tap(loginPageObject.btnLogin);
    }

/*

    @APIService
    public void PaySomeone() {
        onboarding();
        Login();
        tap(By.id("action_transfers"));
        tap(By.id("mTxtTitle"));
        tap(By.id("tv_payee_name"));
        tap(By.id("parentPanel"));
        sendKeysTo(By.id("et_amount"), "100");
        tap(By.id("mBtnNext"));
        tap(By.id("mBtnNext"));
        //swipe the slider
        //size = 1080,1794
        int x_start = (int) (size.width * 0.21); //226
        int x_end = (int) (size.width * 0.90); // 972
        int y = (int) (size.height * .90); //1614
        TouchAction touch = new TouchAction(androidDriver);
        touch.press(x_start, y).waitAction().moveTo(x_end, y).release().perform();

    }



    }

    @APIService
    public void Registration() {
        onboarding();
        int hehe = ThreadLocalRandom.current().nextInt(1000000, 9999999);
        String randomNumberString = "926" + String.valueOf(hehe);

        tap(By.id("tvRegister"));
        isEmptyTextField(By.id("edtFirstName"));
        sendKeysTo(By.id("edtFirstName"), "abcdefg");
        sendKeysTo(By.id("edtLastName"), "abcdefg");
        sendKeysTo(By.id("edtMobile"), randomNumberString);
        sendKeysTo(By.id("edtEmail"), "haha" + randomNumberString + "@haha.com");
        sendKeysTo(By.id("TILPassword"), "Welcome123");
        sendKeysTo(By.id("edtVerifyPassword"), "Welcome123");
    }
*/




    public void isEmptyTextField(By by) {
        androidDriver.findElement(by).sendKeys("a");
        androidDriver.findElement(by).clear();
        try {
            if (androidDriver.findElement(By.id("textinput_error")).isDisplayed() == true) {
                System.out.print("good");
            }

        } catch (Exception e) {
            System.out.print("aw");

        }
    }

    public boolean checkElementExists(By by) {
        try {
            androidDriver.findElement(by).click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @After
    public void End() {
        androidDriver.quit();
    }
}


