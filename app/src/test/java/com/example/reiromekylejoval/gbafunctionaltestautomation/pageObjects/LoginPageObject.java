package com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects;

import org.junit.After;
import org.junit.Before;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by reirome.joval on 2/14/2018.
 */

public class LoginPageObject {

    public LoginPageObject(AndroidDriver androidDriver) {
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver, 5, TimeUnit.SECONDS),this);
    }

    @AndroidFindBy(id = "username")
    public AndroidElement username;

    @AndroidFindBy(id = "password")
    public AndroidElement password;

    @AndroidFindBy(id = "btnLogin")
    public AndroidElement btnLogin;

    @AndroidFindBy(id = "tvForgotPassword")
    public AndroidElement tvForgotPassword;

    @AndroidFindBy(id = "tvRegister")
    public AndroidElement tvRegister;

    @AndroidFindBy(id = "tvSupp")
    public AndroidElement tvSupp;

    @AndroidFindBy(id = "tvATMFinder")
    public AndroidElement tvATMFinder;

}
