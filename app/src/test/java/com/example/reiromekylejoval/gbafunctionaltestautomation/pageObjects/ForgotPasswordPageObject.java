package com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects;

import com.example.reiromekylejoval.gbafunctionaltestautomation.base.BaseActions;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity.ForgotPasswordAPIEntities;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by reirome.joval on 2/13/2018.
 */

public class ForgotPasswordPageObject extends BaseActions{

    public ForgotPasswordPageObject(AndroidDriver androidDriver){
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver, 5, TimeUnit.SECONDS),this);
    }

    @AndroidFindBy(id = "edtMobileOrEmail")
    public AndroidElement txtMobile;

    @AndroidFindBy(id = "btnSubmit")
    public AndroidElement btnSubmit;

    public VerificationPageObject navigateToVerification(AndroidDriver androidDriver){
        btnSubmit.click();
        return new VerificationPageObject(androidDriver);
    }
}
