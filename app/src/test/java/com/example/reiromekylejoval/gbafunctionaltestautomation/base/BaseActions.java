package com.example.reiromekylejoval.gbafunctionaltestautomation.base;

import com.example.reiromekylejoval.gbafunctionaltestautomation.network.APIService;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.RetrofitClient;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.Utility;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity.SingletonFactory;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity.TestAPI;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.LoginPageObject;
import com.example.reiromekylejoval.gbafunctionaltestautomation.pageObjects.OnboardingPageObject;

import org.apache.xpath.SourceTree;
import org.apache.xpath.operations.And;
import org.junit.Before;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by reirome.joval on 2/13/2018.
 */

public class BaseActions {

    protected APIService mAPIService = RetrofitClient.getClient();
    protected AndroidDriver androidDriver;
    protected Dimension size;
    protected String code;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Nexus 5X API 13");
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
        capabilities.setCapability(CapabilityType.VERSION, "6.0");
        capabilities.setCapability("noReset", "true");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("session-override","true");
        capabilities.setCapability("appPackage", "republisys.globalbankaccess");
        capabilities.setCapability("appActivity", "republisys.globalbankaccess.module.presentation.onboarding.OnBoardingActivity");

            androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        androidDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        size = androidDriver.manage().window().getSize();
    }


    /*-----A-C-T-I-O-N-S----------------------------------------------------------------------------------------------*/

    public void tap(AndroidElement androidElement) {
        TouchAction action = new TouchAction(androidDriver);
        action.tap(androidElement).perform();
    }

    public void sendKeysTo(AndroidElement androidElement, String input) {
        TouchAction action = new TouchAction(androidDriver);
        action.tap(androidElement).perform();
        androidDriver.getKeyboard().sendKeys(input);
        if (androidDriver.isKeyboardShown()) {
            androidDriver.hideKeyboard();
        }
    }

    public void swipeLeft() {
        int x_start = (int) (size.width * 0.99);
        int x_end = (int) (size.width * 0.1);
        int y = 300;
        TouchAction touch = new TouchAction(androidDriver);
        touch.press(x_start, y).waitAction().moveTo(x_end, y).release().perform();
    }


    /*-----N-A-V-I-G-A-T-I-O-N-S--------------------------------------------------------------------------------------*/

    public void navigateToLogin(){
        swipeLeft();
        OnboardingPageObject page = new OnboardingPageObject(androidDriver);
        String skiptxt = page.btnSkip.getText();
        while (!skiptxt.equals("Done")) {
            if (!skiptxt.equals("Done")) {
                swipeLeft();
                skiptxt = page.btnSkip.getText();
            }
        }
        tap(page.btnSkip);
        System.out.println("Passed: Navigate to Login");
    }

    public void navigateToForgotPassword(){
        //navigateToLogin();
        LoginPageObject loginPage = new LoginPageObject(androidDriver);
        tap(loginPage.tvForgotPassword);
    }




}
