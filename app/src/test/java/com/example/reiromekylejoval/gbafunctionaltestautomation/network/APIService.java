package com.example.reiromekylejoval.gbafunctionaltestautomation.network;

import com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity.ForgotPasswordAPIEntities;
import com.example.reiromekylejoval.gbafunctionaltestautomation.network.entity.TestAPI;

import junit.framework.Test;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by reirome.joval on 2/14/2018.
 */

public interface APIService {

    @POST("forgot")
    @FormUrlEncoded
    Call<TestAPI> getVerification(@Field("mobile") String mobile);
}
